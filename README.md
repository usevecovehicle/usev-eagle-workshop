# USEV Eagle Workshop

Repository containing files and links associated with the USEV Eagle workshop

If you experience issues opening Eagle, such as it crashing on launch, check out [this troubleshooting guide](https://knowledge.autodesk.com/support/eagle/troubleshooting/caas/sfdcarticles/sfdcarticles/Eagle-crashes-seconds-after-launching-splash-screen.html).

## Presentation
[Presentation Slides]()

Some of the notable points from the presentation are going to be included in the wiki page associated with the presentation.

## PCB Design Showcase
The example PCB designs shown during the workshop are included with some basic analysis in [this document]().

## Supported Tutorials
### Getting Started
Eagle is an Autodesk produce and can be downloaded [here](https://www.autodesk.co.uk/products/eagle/free-download?plc=F360&term=1-YEAR&support=ADVANCED&quantity=1).

## Beginner

## Intermediate
Designing a schematic and PCB layout from a datasheet is a common task. The datasheet for the Analog Devices LT8618 has a great amount of information to help designers develop their hardware. 

As an intermediate challenge, create a schematic and PCB layout for the LT8618 that will output 5 V from a 24 V input. The switching frequency should be set to 1 MHz and a Coilcraft inductor should be used. You may also with to consult the design for the LT8618 evaluation board, this can be accessed via the [LT8618 product page](https://www.analog.com/en/products/lt8618.html#product-overview). The datasheet can be found [here](https://www.analog.com/media/en/technical-documentation/data-sheets/lt8618-8618-3-3.pdf).